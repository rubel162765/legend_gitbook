# Blockchain NFT Technology

#### DLD & BITTO Potion will be deployed on both Binance Chain and Ethereum Chain under BEP-20 & ERC-20. You will be able to trade on both DEX such as [Pancakeswap](https://pancakeswap.finance/) & [Uniswap](https://uniswap.org/) along with other CEX.



Blockchain NFT assets (heroes,guardians,equipment, pets, artifacts, etc) will be under **BEP-721.** Data's will be stored using InterPlanetary File System (IPFS). Future Scaling of Game Mods within DefinityLegend Platform will be introduced soon.



Gaming NFT's will then be migrated cross chain to BSC to be convereted to **in-game playable characters on BSC.**&#x20;

This is only for opensea.io and ethereum based NFT that crosses over $30,000 in value.

[**DefinityLegend**](https://definitylegend.com/) will be built using the UNITY platform and is currently available only on PC. \
An integration to a mobile game will be available via the Apple and Google Appstores in the near future.\
