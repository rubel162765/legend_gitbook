# Team

#### DeFinityLegend is developed by **DeFinity Studio** and powered by **BITTO Tech'**s Blockchain Development. DLD is looking to implement using **Immutable X'**s layer 2 design. This allows for full **synergy** of the teams experience within both gaming and blockchain industries to combine and develop a groundbreaking gaming asset platform merging with NFT blockchain.&#x20;

The team consists of 20+ Full Time members working on all aspects of the game that includes&#x20;

1. **`Game Developers`**
2. **`Game Programmers`**
3. **`Blockchain Developers`**
4. **`Smart Contract Engineer`**
5. **`Front-end UI/UX developer`**
6. **`Server Analyst`**
7. **`Full-Stack Developer`**
8. **`Animator`**
9. **`Concept Artists`**
10. **`2D Illustrator`**
11. **`Lead Game Designer`**

And more.

The team consists of selected individuals with passion, enthusiasm and commitment to making products from scratch to something incredible.&#x20;

### **About DeFinity Studio**

Definity Studio is a newfound team of 10+ team members ( And Growing) consisting of experienced and talented individuals that have more than 25 years of added experience in the gaming industry from designing, creating and developing of the game. With creating a blueprint since January 2021, and developing the game progressively, DeFinity Studio hopes to create a new gaming experience.

### **About BITTO Tech**

BITTO Tech is a leading blockchain solutions company that has been established in 2017 in the UK, with a foothold in Kuala Lumpur, Labuan and London, making it one of the first few blockchain companies standing alongside Binance & Kucoin that was founded in the same year.

### **About Immutable ( TBC )**

Immutable is an industry leading $17m VC-funded blockchain company backed by investors such as Naspers, Galaxy Digital and Coinbase. They are responsible for launching the most successful blockchain Trading Card Game, _Gods Unchained_, which is headed up by Chris Clay, the former director of _Magic the Gathering Arena_. Immutable has also developed Immutable X, the first Ethereum layer 2 designed for NFTs.

For more information on team members refer to this [**blog article**](https://immutablex.medium.com/)****
