# Social Media

⛔️ **Please make sure you are always on the official links listed below. Beware of phishing website! Admins will not initiate to DM you!**&#x20;

### _**Websites**_&#x20;

Definity Legend Website: [https://www.definitylegend.com/](https://www.definitylegend.com/)&#x20;

Definity Legend Whitepaper: [https://whitepaper.definitylegend.com/](https://whitepaper.definitylegend.com/)&#x20;

### _**Social Media**_&#x20;

Twitter: [https://twitter.com/definitylegend](https://twitter.com/definitylegend)&#x20;

Discord: [https://discord.com/invite/xC7qXUzGax](https://discord.com/invite/xC7qXUzGax)&#x20;

Facebook: [https://www.facebook.com/definitylegend](https://www.facebook.com/definitylegend)&#x20;

Telegram News Channel: [https://t.me/definitylegendnews](https://t.me/definitylegendnews)&#x20;

Telegram global group chat: [https://t.me/definitylegend](https://t.me/definitylegend)&#x20;

Telegram Vietnam group chat: [https://t.me/definitylegendvn](https://t.me/definitylegendvn)&#x20;

### _**Marketplace**_&#x20;

Opensea (Official account): [https://opensea.io/DefinityLegend](https://opensea.io/DefinityLegend)&#x20;

Opensea (Limited Edition Founder pack): [https://opensea.io/collection/definity-legend-founders-edition](https://opensea.io/collection/definity-legend-founders-edition)&#x20;

