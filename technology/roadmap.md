---
description: >-
  DLD's world is a custom created game with assets built since June 2021 to the
  present day. While it generally takes about two years to develop a game, we
  have estimated a shorter period
---

# Roadmap

**Q1'22**

* **Founders Sale, Initial Angel Round**
* **Gameplay Walkthrough Release**
* Social Media Building
  * Concept Creation
* Whitepaper



**Q2 22**

* Pre-Seed Sale
* Gameplay Walkthrough
* Strategic Partership



**Q3'22**

*   **Public Round Sale**

    * Collaborative NFT Sale&#x20;
    * Artist NFT Sale
    * Land Sale


*   **Q4'22**

    * **Gameplay**
      * Beta Test Gameplay
      * Land Ownership
      * Guilds Gameplay Campaign Scene
      * Collaboration with Exclusive Brands to bring characters to life in DeFinityWorld.


* **Q1'23**
  * **Gameplay**
    * Combat Improvements
    * Upgrading Equipment
    * Upgrading Guardians
    * Test of Olympus
    * Bowels of Tartarus&#x20;

