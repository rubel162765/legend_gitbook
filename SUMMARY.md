# Table of contents

* [Definity Legend](README.md)
* [🏪 Marketplace](marketplace.md)

## 👾 ABOUT THE GAME <a href="#gameplay" id="gameplay"></a>

* [Concept](gameplay/game-designs.md)
* [Core Gameplay](gameplay/core-gameplay/README.md)
  * [Combat](gameplay/core-gameplay/combat.md)
* [Importance of Elements](gameplay/importance-of-elements/README.md)
  * [Guardians](gameplay/importance-of-elements/guardians.md)
  * [Heroes](gameplay/importance-of-elements/heroes.md)
  * [Equipments](gameplay/importance-of-elements/equipments.md)
* [PvE Battles](gameplay/pve-battles.md)
* [PvP Battles](gameplay/pvp-battles/README.md)
  * [Guild Nation Wars](gameplay/pvp-battles/guild-nation-wars.md)
* [DLD Metaverse Land NFT Asset](gameplay/land-ownership/README.md)
  * [Asset List Documentation](gameplay/land-ownership/asset-list-documentation.md)

## 🌎 Definity Legend World Currency

* [DLD Token](definity-legend-world-currency/dld-coin/README.md)
  * [Utilities of DLD Coin](definity-legend-world-currency/dld-coin/utilities-of-dld-coin.md)
* [BITTO Potion](definity-legend-world-currency/bitto-potion/README.md)
  * [Utilities of Bitto Potion](definity-legend-world-currency/bitto-potion/utilities-of-bitto-potion.md)
* [Legal](definity-legend-world-currency/legal.md)

## 💎 Tokenomics

* [NFT Market Potential](tokenomics/nft-market-potential.md)
* [Tokenomics](tokenomics/tokenomics.md)

## ℹ️ Information <a href="#technology" id="technology"></a>

* [Blockchain NFT Technology](technology/blockchain-nft.md)
* [Team](technology/team.md)
* [Social Media](technology/social-media.md)
* [Roadmap](technology/roadmap.md)
* [DLD Custom Art](technology/dld-custom-art.md)
