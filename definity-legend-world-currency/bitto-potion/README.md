# BITTO Potion

Players collect NFT Characters, Guardians, Equipment and Land, to earn items, rewards and progress which can be sold to other players for BITTO POTION!&#x20;

{% hint style="success" %}
**BITTO Potion** Earned From Game Play
{% endhint %}

![](../../.gitbook/assets/Potion\_2.jpg)
