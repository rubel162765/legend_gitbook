# Utilities of Bitto Potion

{% hint style="info" %}
[**BITTO Potion**](https://whitepaper.definitylegend.com/definity-legend-world-currency/bitto-potion) is the main In-Game Currency of 2d Turn Based Gameplay of DLD, and the main source of player income in DLD 2d Turn Based Strategy. It is earned through combat, trading and other forms of participation in game.
{% endhint %}

### 💎 **Play to Earn**

BP is earned mainly through successful participation in **all forms of** [**combat**](https://whitepaper.definitylegend.com/gameplay/core-gameplay/combat); Campaign, Trials of Olympus, Bowels of Tartarus, Olympic Arena and Nation Wars.

### 💎 **Marketplace** items are also traded with other players, using **BITTO Potion**, such as;

* Armor and Weapons
* Heroes
* Guardians
* Artifacts
* Consumables, such as **** Stamina, Health, Metals, Exp Book ****&#x20;

### 💎 **Fusing**

Fusing NFTs such as Guardians, Armor and Weapons requires BP as a **fusion cost** depending on the **quality tier** of the NFTs being involved.

### 💎 **NFT Improvements**

When an NFT is being statistically improved, BP is required as an upgrade cost. The upgrades that require BP are;

* **Evolving NFTs - Armor pieces will improve appearances as the level gets higher (Level 25,50)**
* **Enhancing NFTs - Armor Pieces will gain stronger attributes and stronger visual upgrades when successfully enhanced to a higher level.**
* **Levelling up Guardians**&#x20;

### 💎 **Guilds**

Both fees for **joining** guilds and **upgrading** guilds require BITTO Potion.



{% hint style="info" %}
**"70% of the BITTO tokens used within the NPC world such as evolving, enhancing, will be burned, reducing supply and increasing BITTO's value by having a deflationary system in place".**
{% endhint %}



{% hint style="success" %}
### Summary of Utilites

1. Main Income in 2D Turn Based Game
2. Marketplace Trading Currency for 2D
3. Fusing NFTs
4. Evolving NFTs
5. Enhancing NFTs
6. Leveling up Guardians
7. Joining Guilds
8. Upgrading Guilds
9. Nation War Bid


{% endhint %}



\
