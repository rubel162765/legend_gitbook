---
description: DLD token is the main token utility within the full Definity Legend Eco-System
---

# DLD Token

**DLD Token** can only be bought and used for new launches, treasure chests, events and DLD merchandise within the DefinityLegend world.

![DLD Coin](<../../.gitbook/assets/DLD\_LOGO\_HANDOVER \_2\_ small.png>)



Example Unique Events and ART Collaboration



![Bored Ape To Go Live In DLD Game](<../../.gitbook/assets/Happy Ape.gif>)

