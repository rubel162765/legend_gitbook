# Utilities of DLD Coin

{% hint style="info" %}
****[**DLD Coins**](https://whitepaper.definitylegend.com/definity-legend-world-currency/dld-coin) is the primary utility currency that can be used within the whole eco-system to trade for a variety of items in game. The most common use is acquiring new assets and reward system within the World Game Editor Platform.
{% endhint %}

![DLD Epic Chest](<../../.gitbook/assets/silver\_chest (2).png>)

### 💎 Treasure Chests

Treasure chests contain **Heroes, Armor Pieces, Weapons, Guardians and Consumables**. Their prices in DLD go up, as more chests are opened by players. Monthly updated events, and new Armor Releases & guardians can only be obtained using DLD coins.

An **important** factor is that the armor that comes out of chests are single pieces.&#x20;

{% hint style="warning" %}
**Full sets** of armor are **ONLY** sold directly during the **Limited Sale Event**.&#x20;

Future Sponsorships loans can only be done with Full Set armors & Guardians to earn BITTO Potions.
{% endhint %}

![](../../.gitbook/assets/final\_card.png)



### 💎 **Summoning**

DLD coins are also used for summoning. Both **Heroes and Guardians can be summoned** from their respective buildings.

![Building for Summoning Guardians](../../.gitbook/assets/GuardianSummon.png)

There are **different DLD costs** for summoning **different Tiers** of Heroes and Guardians.



### 💎 **Guilds and Nation Wars**

Guilds are **only** available during limited sales and in **limited numbers**. They can be purchased using DLD coins.

Additionally, **bidding to participate** in Nation Wars, which allow guilds to contest lands, are also done with DLD coins.

### 💎 **Billboard Rental**

Lands **can be rented out** for placing ad images, by interested Advertisers and/or Publishers. These images will be advertising to all players that are taking part in Land Gameplay.

Cost of rental is covered through DLD coins. 80% of the DLD paid for rental goes to the **land owner**.

### 💎 Rewards

In addition to the **rewards** from **Land Ownership**, DLD is also given as a reward to the **Top 10 Guilds** in Nation War.

### Utility Summary

{% hint style="success" %}
1. Treasure Chests
2. Summoning Heroes
3. Summoning Guardians
4. Guild System within DefinityLegend Sandbox Mode
5. Land Ownership Rewards
6. Purchase of Building Materials For Game Creation
7. Top 10 Guild Rewards
8. Bidding for NFT's to be implemented within DLD Game Mods
9. Purchase of in-game boosters, perks, tiered membership within DLD Game Mods
10. Billboard & Banner Rentals
{% endhint %}
