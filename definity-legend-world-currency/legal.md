---
description: https://definitylegend.com/
---

# Legal



DLD are being created, issued and distributed by DefinityLegend for use within the World Of Definity Legend Pc Game while the World Of Definity Legend Pc Game is being developed and distributed by Leonis Pty Ltd. Other than collaborating to bring DLD into the World Of Definity Legend Pc Game, DefinityLegend is not affiliated with World Of Definity Legend or our related entities. DefinityLegend have committed to bring the DLD technology to the World Of Definity Legend Pc Game and we are making the game compatible with the token.

DLD are not NFT’s - they are ERC-20/BEP-20 tokens that are tradeable on any Ethereum or Binance marketplace and neither DefinityLegend nor World Of Definity Legend have any direct control over the transfer of the DLD.

DLD should not be viewed as an investment or speculative asset. They may have value in game or for collectors over time but that value may be limited or non-existent if the World Of Definity Legend Pc Game lacks acceptance, use and adoption, which may have an adverse impact on the use and popularity of the DLD. If, following your own independent advice and due diligence, you do decide to hold DLD as a form of investment on a speculative basis or otherwise, or for a financial purpose, with the expectation or desire that their inherent, intrinsic or cash-equivalent value may increase with time, you assume all risks associated with such speculation or actions, and any errors associated therewith, and accept that the DLD are not being created, issued and distributed by DefinityLegend or its affiliates for investment or speculation.
