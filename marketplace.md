---
description: Tutorial on DefinityLegend marketplace
---

# 🏪 Marketplace

{% hint style="success" %}
### First NFT Game To Implement NFT Art Design Into The Gaming World!
{% endhint %}

DLD's NFT Marketplace is where you create, sell, and buy NFTs digital content. Moreover, you can launch NFT auctions, earn commissions from affiliate programs, and build your favourite NFT collection.

To promote, support NFT & Art creation, DLD Studio will implement selected character designs will be into the DeFinityLegends world in-game! Making it a Marketplace that promotes the creation and limited variety in DLD's world!.

![Marketplace Interface of DLD](.gitbook/assets/Marketplace.png)

{% hint style="warning" %}
More coming soon!&#x20;
{% endhint %}

###
