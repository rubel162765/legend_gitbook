---
description: >-
  This list provides an insight into the game matrix where NFT's can be migrated
  from 2D turn-based DLD Game. This also shows the Props and asset creation that
  are required in DefinityLegend Sandbox.
---

# Asset List Documentation

**Updating as at 24.05.2022**\
****\
****The smallest usable piece of land in the sandbox dimensions 96x128x96 meters. Quite enough to prepare a level. By taking these measurements as an example, the measurements of the assets we need will be adjusted accordingly.

All camera angles, except isometric, will be player-focused. There will be 4 different camera angles in the editor. FPS - TPS – Top-down – Isometric

There will be 2 different lighting settings, sunrise and night.

The dimensions are written as WxHxL.

&#x20;

**Environment Props**

**Trees** - At least 2 varieties for all climate and all terrain types. And 2 different models for each variety.

&#x20;               Palmtree -  Pine -  Oak -  Cherry blossom... etc.

**Pyramidal** -  Body: 1x7x1   Branches and leaves:   It starts at 4 and progresses decreasingly.

**Columnar** - Body: 1x10x1   Branches and leaves: It starts at 3 and progresses decreasingly.

**Round** - Body: 1.5x5x1.5   Branches and leaves: Starts and ends with 5

![](file:///C:/Users/USER/AppData/Local/Temp/ksohtml/wpsF7EB.tmp.jpg)&#x20;

**Plants** – Plants such as grass, bush, flower, and cactus species

4 different types of grass and bush for 4 different climates. (long - 0,7x2x0.7 and short - 0.5x1x0.5)

2 types of cactus and 2 different models for each variety (1x3x1)

4 types of flower (It depends on the variety. But a standard flower seedling measures 0.7x1x0.7)

&#x20;

**Rocks** – 4 types of rocks in different shapes. There will be big and small of each shape.

**Single rock and rock chunks** - Small (1.5x1x1.5)  Big (3x1.5x3)

**Rockpile** - Small ( 5x3x5) Big (10x6x10)

&#x20;

&#x20;             **Buildings**

**Castles** - Fort, keep, and castle made of stone or wood. 5 separate castles representing the elements alongside the standard castle variants.

**Fort** (9x8x9)

**Keep** (15x10x15)

**Castle** (18x12x18)

&#x20;

**Warehouses** - For storage areas for materials in the editor. 4 different sizes. With each increase in size, the model gets a little bigger and gets some minor additions.

**Small** (3x3x3)

**Medium** (3.5x3x3.5)

**Large** (4x3.5x4)

**Huge** (5x4x5)

&#x20;

**Towers** - 5 different towers representing the elements. (5x6x5)

&#x20;

**Houses** - 1 different house for every element (4x3x3)

&#x20;

**Bridges** - Made of stone or wood. 3 different models. Short, long, and corner.

**Short** (2x1.5x2)

**Long** (2x1.5x4)

**Corner** (2x1.5x2)

&#x20;

**Walls and arch gates** - Made of stone or wood.  1 different wall and arch gates with each type of stone to be used. (3x3x1)

&#x20;

**Doors** - 3 different door models. (3x3x0.3)

&#x20;

**Units, Enemys, NPC and Friendly Characters**

Every character here can be used as in the title.

&#x20;

**Humanoids:**

The measurement of all humanoid characters is 1x1.5x1

All humanoids can use the same set of animations and similar models.

2 for each element, 10 heroes in total. Maybe one of them can be range and the other can be melee hero.

At least 20 NPCs that can be shopped, provided information in the game, used to create crowds, and healers or buffers.

&#x20;****&#x20;

**Non-humanoids:**

16 animals in total, 2 different for each season and climate. 5 animals representing each element.

Design ideas of all enemies, guardians, and bosses in the 2d part can be used.

&#x20;

**Collectible items and types of equipment**

**Weapons:** All weapons come from the elements except the standard weapon set.

2 weapons for each element. 10 weapons in total.

Besides 5 swords, other weapons can be separated according to the properties of the elements.

For example, it can be the fire element's axe, the air element's staff, or the earth element's sledgehammer.

Standard weapon types are sword, axe, or spear.

&#x20;

**Armors:** As in the 2d part, 5 different armor pieces can be used.

There are 2 of each armor piece for each element.

Each set is in the form of two armor sets, light and heavy.

Standart armor pieces are shield, helmet, gauntlet, leggings, and breastplate. And only available as a light set.

&#x20;

**Collectible items**

&#x20;****&#x20;

**Artifacts:**

Each nation can have one artifact to represent itself.

These artifacts can grant the player special powers. Or it may be the quest of the level is to simply search for these artifacts.

Keys to open doors.

&#x20;

**Edibles:**

Plants that give the player health or extra powers when consumed. 5 different fruits and vegetables will be enough.

&#x20;

**Interactable objects:**

A lever for opening a door.

3 types of openable chests.

Stairs

Torches and lanterns that we can light and extinguish.

Nests and dens that constantly spawn new enemies.

&#x20;

Textures

Abilities

VFX\
\
\
\
