# DLD Metaverse Land NFT Asset

### The initial genesis 23xx lands will be sold.&#x20;

![As land is upgraded, the % earning increases](../../.gitbook/assets/water\_terrain\_ratio\_adjustment\_and\_layout\_finalize.jpg)

### NFT Land Profit Sharing

67% of the profits made from advertising Billboards will be shared with Land Owners.

Lands that are within Zone 1 & 2 to the Center of the Advertising area will get a 30% & 20% share of the revenue. Zone 3 & 4 will share 10% and 7% ( from users advertising on a billboard in the centre )

Lands will be used to earn resources, materials, blueprints and passive income for in-game gold currency which can be used to upgrade the land plots to increase land points.

Higher land points will increase the % earned from the advertising area % probability of DLD token drop.&#x20;

_**Zone 1**_ - will have `360 lands`

_**Zone 2**_ - will have `504 lands`

_**Zone 3**_ - will have `648 lands`

_**Zone 4**_ - will have `864 lands`



### Advertisement Tracking For Advertisers (CPC)

Users can claim 3% of the billboard revenue by logging in daily by clicking on the billboard.&#x20;

This allows the billboard advertiser to gauge daily views and awareness based on a CPC indicator.

## **What are the dimensions of each plot of Land?** <a href="#8876" id="8876"></a>

![Mansion - Access To Land Management](../../.gitbook/assets/Mansion.png)



Every plot of land consists of 36 blocks (6x6)



Land will be used for crafting, building, war, tower defense. ( More info coming soon)

![DLD Tower Defense](../../.gitbook/assets/landscape\_skecth\_3.jpg)

![DLD Elemental Towers](../../.gitbook/assets/towers\_sketch.jpg)

![](../../.gitbook/assets/neutral\_towers\_white.jpg)



**Resource Gathering and Crafting**

**coming soon.**

****

****
