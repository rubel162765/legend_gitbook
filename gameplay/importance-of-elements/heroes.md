# Heroes

{% hint style="info" %}
Each hero has their own element, to know more about "the elements" please refer 👉 [here](https://whitepaper.definitylegend.com/gameplay/importance-of-elements)
{% endhint %}

![Full Custom Element Armours Sets](../../.gitbook/assets/frame\_done.jpg)

#### Heroes are avatars who have certain attributes and skills that enable them to partake in combat.

Heroes are the main player character and their teammates. These are the characters that players equip and deploy in combat. The combat party consists of the main hero, and up to 5 other heroes.\
****

**Each Hero comes with the following attributes:**

* **Health**: The total health pool of the Hero. The hero becomes incapacitated and unavailable for combat when it reaches 0.
* **Attack**: Total physical attack power of the Hero.
* **Defence**: Total physical defensive power of the Hero.
* **Magical Attack:** Total magical attack power of the Hero.
* **Magical Defense:** Total magical attack power of the Hero.
* **Elemental Affinity**: Attunement of a hero to a particular element. Each hero can attune to a single element, except the Main Player Hero who is attuned to all elements.

Health, Attack and Defense can be increased by levelling up heroes or acquiring better equipment. Elemental Affinity can only be increased by fusing existing heroes to create improved versions.

{% hint style="success" %}
👾 **In addition to simple attack and defence, each hero has 4 additional skills that define their combat output. These skills can also be levelled up using resources.**
{% endhint %}
