---
description: >-
  During the Founders sale, seize the opportunity to obtain full set armours!
  Only full set armours can be rented out for sponsorship programs in the
  future.
---

# Equipments

{% hint style="info" %}
Each equipment has their own element, to know more about "the elements" please refer 👉 [here](https://whitepaper.definitylegend.com/gameplay/importance-of-elements)
{% endhint %}

#### Players can gather various types of items in Definity Legends. Main Categories of items are;

* _****_[_**Weapons**_](https://whitepaper.definitylegend.com/gameplay/importance-of-elements/equipments#weapons)_****_
* _****_[_**Armor**_](https://whitepaper.definitylegend.com/gameplay/importance-of-elements/equipments#armor)_****_
* _**Artifacts (coming soon)**_
* _**Consumables (coming soon)**_

Items such as Weapons and Armor are element specific. They are ideally equipped by a hero which has the matching elemental affinity. This will provide additional benefits to the heroes that are facing opposing elements, in form of additional damage and defence.&#x20;

Each item has qualities that are divided into tiers. There are 6 tiers in Definity Legends;

* **Common `(Non-NFT, Non-Tradable)`**
* **Uncommon : `15%` drop rate from chests.**
* **Rare: `12%` drop rate from chests.**
* **Very Rare: `9%` drop rate from chests.**
* **Epic: `5%` drop rate from chests.**
* **Legendary: `3%` drop rate from chests.**

**"These percentage drop rates are based on 1 single full armor set. The drop rates will decrease significantly as the amount of armor sets increase"**

Common items are non-NFT items that players start the game with and can be rewarded in the beginner stages of the game. They cannot be traded in the marketplace.

## Weapons

Weapons are equipped by heroes to increase their Attack stat. They are split into tiers to signify their strength.

## Armor

Armors effect the Defence stats of heroes. They have tiers just like weapons. Armors can additional grant bonus stats, and even skills when the hero equips them as a set.

Available types of armor in the game are;

* Shield
* Helmet
* Gauntlet
* Leggings
* Breastplate

Out of all the armor types, shield is the only optional slot, which can equip a secondary weapon instead of a shield, to increase attack rather than defence.

{% hint style="success" %}
## Enhancement - for Weapons & Armor
{% endhint %}

**Weapons and Armor** can be enhanced using Metals and BITTO Potion. Enhancing the items provide an increase in stats and also gain visual changes. There is a chance of failure during enhancement, which can be reduced by using higher metals for enhancement.

![](../../.gitbook/assets/table\_again\_1.gif)

* **Enchancement `+1-3` = `Blue Aura`**&#x20;
* **Enhancement `+4-6` = `Green Aura`**
* **Enhancement `+7` = `Gold Aura`**
* **Enhancement `+8` = `Purple Aura + Soft Lightning` **&#x20;
* **Enhancement `+9` = `Purple Aura + Hard Lightning` **&#x20;
* **Enhancement `+10` = `Red Aura + Soft Lightning` **&#x20;

#### There are 5 Metals in the game;&#x20;

* **Copper Ingot (`Used for +1 - +2`)**
* **Iron Ingot (`Used for +3 - +4`)**
* **Mithril Ingot (`Used for +5- +6`)**
* **Titan Ingot (`Used for +7 - +8`)**
* **Aetherium (`Used for +9 - +10`)**&#x20;

**(Metals provides up to `5% bonus`if used in lower enhancements)**

![Blacksmith Refining](../../.gitbook/assets/Capture.JPG)
