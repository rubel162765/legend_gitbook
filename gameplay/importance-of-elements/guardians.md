# Guardians

{% hint style="info" %}
Each guardian has their own element, to know more about the elements please refer 👉 [here](https://whitepaper.definitylegend.com/gameplay/importance-of-elements)
{% endhint %}

![Guardians Summoning Shrine](../../.gitbook/assets/guardian\_summon.jpg)

Guardians are elemental beings that can be summoned into battle by players. Once summoned, they automatically battle alongside the player until they are defeated or the battle ends.

Guardians are similar to heroes in that they also have HP, Attack, Defense and Skills. But unlike heroes, they cannot equip items, cannot be fielded instead of heroes and have only single skills.

## Classifications

There are _**3 Tiers**_ of Guardians:

1. **Ultra Rare: `65%` Elemental Affinity**
2. **Epic: `70%` Elemental Affinity**
3. **Legendary: `80%` Elemental Affinity**
4. **Mythic: `84%` Elemental Affinity (To be added at a later date)**

## **5 Types of Guardians with fixed elements**

Guardians are divided into 5 **fixed** elements.

### DEMON - Fire element

Demons, the elementals of fire, then taught the sages to manipulate the fiery power to melt and mold metal. The sages of Ignis became great smiths, learning to create metal tools, weapons, and armors.

![Demon Guardian - Fire](<../../.gitbook/assets/Demon DLD Guardian.png>)



### GOLEM - Earth Element

Golems, the elementals of earth, taught the sages how to manage different types of stone and earth. The sages of Petra became great builders, learning to create walls, houses, and fortifications.

![Golem Guardian - EARTH](<../../.gitbook/assets/DeFinity Legend Guardian Golem.png>)



### SLYPH - Wind Element

Sylphs, the elementals of air, taught the sages to channel the subtle energies of air, which inspired creativity and imagination. The sages of Ventum became great inventors, conceiving writing and music.

![Sylph Guardian - Wind](<../../.gitbook/assets/sylph animation (2).png>)



### DRAGON - Water Element

Water Dragons, the elementals of water, taught the sages the inner (deep) and outer (shallow) manipulation of the water. The sages of Aqua became great healers and invented agriculture.



![Dragon Guardian - WATER](<../../.gitbook/assets/DeFinity Legend Guardian Dragon.png>)



### ARCHANGEL - **Aether Element**

The mercenaries, who named their army of "Army of the Chosen", joining large numbers of their fellows for the first time, pooled their powers and prior knowledge of the other elements, creating a powerful ritual that allowed some of them to summon, for the first time in history, the power of the planet's supreme guardians, the archangels.&#x20;

The Battle of the Cosmic Plains didn't need to be won. The mythical guardians' first displays of power were enough for the enemy armies to retreat, running for their lives.&#x20;

The stories say that the symbols painted on the ground by the Aetherians to summon the first Archangels were eternally imprinted on the rocks of the Hills, and, to this day, any inhabitants of Petra and Aqua would rather die than set foot there again**.**

**Archangel is the only Guardian with rejuvenation powers given.**

****

![Archangel Guardian - Aether](<../../.gitbook/assets/Guardian Archangel Aether.png>)
