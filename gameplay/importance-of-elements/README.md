# Importance of Elements

{% hint style="info" %}
The world of [Definity Legends](https://definitylegend.com/) is governed by **5 Elements**. These elements shape all manner of things; people, items and land.
{% endhint %}

Each of these elements has advantages over another in a conflict, with the exception of _**Aether**_, the supernatural element that has a slight advantage over all of them.

![Conflict advantages of elements over each other](../../.gitbook/assets/Elementals\_resized.jpg)

Each character and equipment have an Elemental Affinity that determines the amount of advantage they have over the affected element. Each character or equipment has a fixed affinity that cannot be upgraded. Elemental Affinities can only be improved through fusion.

![Elements Illustration](<../../.gitbook/assets/Screenshot 2021-09-26 at 9.47.46 PM.png>)











