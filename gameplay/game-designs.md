---
description: Game pillars explained in detail
---

# Concept

![In-Game Official Logo](../.gitbook/assets/definity-legends-slogan-transparent.png)

#### [Definity Legend](https://definitylegend.com/) is a Turn-Based Strategy RPG with a blockchain economy. Players engage in battles with monsters and other players using a team of heroes in a fantasy setting.

As players progress through the game, they will acquire better equipment, heroes, guardians and have access to other features such as Bowels of Tartarus, where fearsome boss fights await!

Main Gameplay Features:

* **Command a team** of heroes against bloodthirsty monsters in a gripping campaign or battle other players in PvP contests.
* Don't fight alone! **Form a guild** with other players and wage wars against other guilds to conquer lands!
* **Acquire and Upgrade** heroes to form the best battle squad that you can.
* **Earn NFT rewards** that can be used to get even stronger or traded for **BITTO potions**.
* Overcome the most difficult challenges Olympus has to offer by **defeating the biggest bosses** in Definity Legends universe, to earn **even better rewards**.
* All NFT items can be **traded in DLD marketplace!**\


