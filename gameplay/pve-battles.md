# PvE Battles

![](<../.gitbook/assets/unknown (5) (1).png>)



PvE Battle are the heart of combat gameplay in Definitely Legends. Players will be fighting against increasingly challenging opponents across several different events.

There are **3 main events** where the players can engage in PvE battles;

* ****[**Campaign**](https://whitepaper.definitylegend.com/gameplay/pve-battles#campaign): The Main PvP Event
* ****[**Test of Olympus**](https://whitepaper.definitylegend.com/gameplay/pve-battles#test-of-olympus): Arena Type Battles
* ****[**Bowels of Tartarus**](https://whitepaper.definitylegend.com/gameplay/pve-battles#bowels-of-tartarus): Boss Events

### Campaign

![Campaign](../.gitbook/assets/Campaign.png)

The campaign is the main battle area for PvE fights. It is a story-driven linear progression of battles, fought against common, rare, legendary and epic monsters.&#x20;

Each campaign chapter consists of 9 regular fights and a boss fight, in which higher tier monsters appear. Players can win battles and complete chapters to earn rewards such as **BITTO potions and Metals** for upgrading their armor.



![](<../.gitbook/assets/DLD monster Mushroom.png>)

### Test of Olympus

Test of Olympus is a battle arena, where players can fight previously defeated campaign monsters, to earn tickets that enable them to participate in the Bowels of Tartarus.

The monster that appears in the arena will be tougher than they appear in the campaign. They will also reward BITTO Potions and Metals like campaign mode does.

### Bowels of Tartarus

The most difficult PvE fights in the game are fought in the Bowels of Tartarus, where the players engage in battles against the most powerful bosses in Definity Legend. Winning these battles grant the user some of the best rewards in the game.
