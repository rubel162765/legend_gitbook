---
description: https://definitylegend.com/
---

# Core Gameplay

![Summoning - Earth Guardian](../../.gitbook/assets/earth\_i.png)

### Overview

* Players begin the game with heroes and equipment that they can acquire through the marketplace, treasure chests or limited sales.
* Each hero has an element they are attuned to. These elements not only affect their **performance** against other enemies of different elements, but also affect what type of armor and weapons they can equip.
* There are **5 elements** in Divinity Legends; **Water, Fire, Earth, Air and Aether**. Each of these elements also has **Guardians** that the players can acquire, and have them provide support for their heroes in combat. Once the player has all their heroes ready for battle, along with the guardians, the player can decide where to battle.
* There are many encounters available in the Campaign mode, which is are the main **PvE encounters**. Additional PvE encounters are Test of Olympus, where players can arena-style to earn tickets for Bowels of Tartarus. Bowels of Tartarus has the most difficult bosses in the game, where players can earn.&#x20;

![](<../../.gitbook/assets/Store (1).png>)

### **Core Loop**

* The Core Gameplay of Definity Legends is combat. Players form a team of heroes, up to 4 heroes, and take them into battle against monsters in campaign mode, boss fights in Bowels of Tartarus, or against other players in **PvP battles and Nation War**.
* All combatants have **Attack and Defense** attributes that determine the outcome of battles. In addition to these attributes, combatants have **skills** that can be used in combat to provide additional actions and depth into combat gameplay.&#x20;
* The **heroes can also be equipped** with armor (Helmet, Breastplate, Gauntlets, Leggings and Shield) and a weapon of their choice. **All armors and weapons in the game are limited edition items! No Same armor or weapon set will be released ever again!**
* Through success in combat, players earn rewards that can be used to upgrade their heroes, guardians and equipment or gain new ones. Through improvement, they will be able to **defeat stronger enemies and earn bigger rewards**.&#x20;
