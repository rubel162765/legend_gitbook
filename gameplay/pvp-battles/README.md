# PvP Battles

![PVP with Guardians](<../../.gitbook/assets/unknown (6).png>)

PvP Combat in DLD is an asynchronous affair, where the player who initiates the Battle, fights against an offline team formed by their opponent. Just like PvE, these battles **give out NFT rewards** such as Metals and Bitto Potion. Thus, players can only participate in a limited amount of PvP matches every day.

There are **two main PvP** content in DLD;

For those who want to test their strength against other players, **Olympic Arena** awaits! The Arena offers both daily ranked fights for players, as well as tournament events that give additional rewards. The **higher** the ranking, the more the rewards!

Players looking for more challenging Player vs Player action can join a guild and dive into [**Nation Wars**](https://whitepaper.definitylegend.com/gameplay/pvp-battles/guild-nation-wars)**!**&#x20;
