# Guild Nation Wars

![Guild Nation War Map](../../.gitbook/assets/nation\_war.jpg)

## Guilds

Guilds can be founded through deeds that can be purchased by the players. Once founded, other players can be invited into the guilds to form the ranks. Guilds can wage wars against each other to conquer the territories of Definity Legends.

Guilds depending on the number of lands conquered, will provide **additional buffs** to their guild members. Guild owners will also be able to charge a weekly fee paid by guild members according to rankings.

Guilds can vary in size depending on the acquired deed. There are currently 4 guild sizes available:

* **Level 1** - Company: `20 Players`
* **Level 2** - Brigade: `30 Players`
* **Level 3** - Legion: `40 Players`
* **Level 4** - Corps: `50 Players`

![Map of the Nation](<../../.gitbook/assets/Territory War.jpeg>)

### Nation War

![](<../../.gitbook/assets/Map of Nations.png>)

Guilds can bid for conquering land using [**DLD tokens**](https://whitepaper.definitylegend.com/definity-legend-world-currency/dld-coin). If the guild already has a position on the map, the guild is only allowed to attack adjacent territories next to the guilds own land. There are **66 land plots** in Nation War, divided into 5 elements, each with their own home base for a total of **5 home bases**.&#x20;

When a guild **joins the war** against other guilds for the region, its members can battle the opposing guild members in asynchronous **PvP matches**. Defeating opponents in battles **grants points** according to their ranking in their respective guilds.&#x20;

When the battle is over, the winning guild **takes the land** and hold it till the next **weekly** guild war, where another guild may bid to attack the land held.

**80% of the tokens** paid by the highest bidder will be used as **daily payments** for the Territories held.
