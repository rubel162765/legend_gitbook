# NFT Market Potential

### How much do people spend on average on RPG games on mobile

Almost a third of gamers (28.8%) spending over $500, the average amount spent is a surprising $229.

**U.S. Mobile RPG Spending Rises by 33% Y/Y to $2 Billion in First Eight Months of 2020.**

{% hint style="info" %}
Sources **:** [https://sensortower.com/blog/us-mobile-rpgs-2-billion-revenue](https://sensortower.com/blog/us-mobile-rpgs-2-billion-revenue)
{% endhint %}

## **Introduction**

Although the NFT adventure on Ethereum officially began in 2017 with Cryptopunks, the first “early adopters” of blockchain gaming and CryptoKitties appeared after the crash of the crypto market in early 2018.

## What does the NFT Ecosystem look like after the Boom?

![https://nonfungible.com/blog/nft-ecosystem-look-like-after-the-boom](<../.gitbook/assets/image (2).png>)

While the active wallets between the “before” period (from September to December 2020) and “Pre-Boom” (January to February 2021) numbered 3,107, **there were 6,418 active over the two consecutive periods “Pre-Boom” and “Boom”** (March to April 2021) an increase of 106.5%.

This indicates a good proportion of new users who became interested in NFTs at the very beginning of the 2021 hype then remained active over the following months.

In contrast to the previous downward trend, **the number of active wallets over the  “Boom” period to “Post-Boom” saw an increase of 133.7%!**

## **Conclusion**

We are still only in the middle of the year but what has attracted our attention the most in this analysis is undeniably **the number of wallets which have remained active after the initial hype** that we experienced for a few weeks.

The USD indicator is certainly always interesting to observe but **looking more deeply at the number of active wallets and especially new wallets** gives much more precise indications on the state of the NFT market.

Despite **a difficult context** for all these newcomers to understand, the sector has nothing to do with previous years and has become **much more accessible thanks to the efforts of the entire ecosystem!**

These efforts seem to be rewarded today by **a growing number of NFT users** and **the least we can do today is to welcome them on this adventure** which, in our opinion, is still in its infancy.

&#x20;The few conclusions to be drawn from these Post-Boom analyses are therefore:

* The NFT ecosystem has a very good retention rate of new users (25%)
* After the Boom and hype, the ecosystem has seen nearly 40,000 new users arrive every 2 months.
* The average price of assets in the market has increased significantly, giving rise to a segment of “very expensive” assets that only an elite can acquire.

There is undoubtedly a before and after the Boom. **The ecosystem is growing faster, appealing to more and more new users, who have already begun to shape this new era of the NFT ecosystem.**

![https://nonfungible.com/blog/q2-2021-nft-report](<../.gitbook/assets/image (3).png>)

{% hint style="info" %}
**Information written here is fully credited to** [**https://nonfungible.com/blog**](https://nonfungible.com/blog)****
{% endhint %}

![https://marketplace.axieinfinity.com/axie?mystic=1](<../.gitbook/assets/axie infinity.JPG>)

Axie launched their mystic for an [estimate $25](https://coinranking.com/nft/ecb0022427-axie-infinity-rare-mystic) at around November 2020

Today their lowest-priced Mystic 1 is sold for an estimated value of _**$87,000**_**.**
