---
description: This Page shows the indepth structure of DLD tokenomics.
---

# Tokenomics

## TOKEN SALE (updated as at 7.4.2022 )

### Seed Funding (10% of tokens )

• Allocation: 30 Million #DLD

• 1 #DLD = $0.025

‌• Raise : $750,000&#x20;

‌‌• 2% at TGE, 4 months cliff, 6.125% per month for 16 months.

### Private round (16% of tokens )

Allocation: 48,000,000 **** #DLD

• 1 #DLD = $0.05

• Raise : $2,400,000

• 4% at TGE - 3 months cliff -  6% per month for 16 months.

### Public Round Launchpads (3.778% of tokens)

Allocation: 11,333,000 #DLD&#x20;

• 1 #DLD = $0.075

‌• Hard cap: $850,000

‌• 25% at TGE, then 15% per month for 5 months.

• Date: TGE estimated at July-August 2022 (updated as at 7.4.2022)



### **TOKEN at TGE**

1. **Seed ( 600,000 DLD )**
2. **Private ( 1,920,000 DLD )**
3. **Public ( 2,833,250 DLD )**&#x20;

**Estimated Total Circulating supply at TGE = 5,353,250 Million DLD**

#### **Total Market Cap at TGE = $401,500**

### Total Token Supply = 300,000,000 DLD



## DLD Token Allocation

| **Stages**                  | Percentage  | # Amount of DLD  | Total Hardcap                                  | Vesting                                                  |
| --------------------------- | ----------- | ---------------- | ---------------------------------------------- | -------------------------------------------------------- |
| Seed Funding                | 10%         | 30,000,000       | $750,000 (In Progress)                         | ‌‌4 months cliff, 16 months of 6.125% per month.         |
| Private Round               | 16%         | 48,000,000       | <p>$2,400,000 ($1 Million</p><p>Committed)</p> | 4% at TGE - 3 months cliff - 6% per month for 16 months. |
| Public Sale                 | 3.778%      | 11,333,000       | <p>$850,000</p><p>(In Progress)</p>            | 25% at TGE, then 15% per month for 5 months.             |
| Advisor                     | 6%          | 18,000,000       |                                                | 8 months cliff then 8.33% per month for 12 months.       |
| Team                        | 16%         | 48,000,000       |                                                | 12 months cliff then 5% per month for 20 months.         |
| Marketing & Partnership     | 6%          | 18,000,000       |                                                | 8.333%% per months for 12 months.                        |
| Liquidity and exchanges     | 6%          | 18,000,000       |                                                | 50% at TGE, then 6.25% per month for 8 months            |
| Treasury                    | 11.222%     | 33,666,000       |                                                | 4.166% per month for 24 months                           |
| Game Incentives For Players | 15%         | 45,000,000       |                                                | 5% per month for 20 months.                              |
| **Total  Supply**           | **100.00%** | **300,000,000**  | $4,000,000                                     |                                                          |





