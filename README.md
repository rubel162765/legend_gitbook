---
description: >-
  Official Whitepaper, last updated February 2022. All contents are subject to
  change. (Best to view on PC Desktop for optimum visual experience)
---

# Definity Legend

![](.gitbook/assets/leaderboard\_render\_less\_obvious.jpg)

## Introduction <a href="#introduction" id="introduction"></a>

Welcome to the fantasy world of [DeFinity Legend](https://definitylegend.com/), where you can enjoy a gaming experience and earn money simultaneously!

With DeFi and NFT spearheading the space, Definity Legends is a blockchain based Role-Playing Game which combines the elements of DeFi, NFT & Play2Earn in one. The first to combine potential NFT art listed within DLD marketplace to come to live within the DLD world!



